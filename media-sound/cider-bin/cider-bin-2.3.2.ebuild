# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop unpacker xdg-utils

DESCRIPTION="A new cross-platform Apple Music experience based on Electron and Vue.js written from scratch with performance in mind."
HOMEPAGE="https://github.com/ciderapp/Cider"
SRC_URI="http://files.niels.sh/cider_${PV}_amd64.deb"
S="${WORKDIR}"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND=""
IDEPEND="
	dev-util/desktop-file-utils
	dev-util/gtk-update-icon-cache
"

src_install() {
	insinto /opt
	doins -r opt/Cider
	fperms 755 /opt/Cider/chrome_crashpad_handler
	fperms 755 /opt/Cider/chrome-sandbox
	fperms 755 /opt/Cider/cider

	domenu usr/share/applications/cider.desktop

	local x
	for x in 16 32 64 128 256 512; do
		doicon -s ${x} usr/share/icons/hicolor/${x}*/*
	done
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

